.. Mercedes Benz Greener Manufacturing documentation master file, created by
   sphinx-quickstart on Thu Jun 30 23:08:27 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Mercedes-Benz Greener Manufacturing's documentation
===============================================================

   **Mercedes-Benz Greener Manufacturing**

   *Can you cut the time a Mercedes-Benz spends on the test bench?*

   Mercedes Benz offers customers the option to customise their cars. To ensure the safety and reliability of each
   and every unique car configuration before they hit the road, Daimler’s engineers have developed a robust testing
   system. But optimizing the speed of their testing system for so many possible feature combinations is complex
   and time-consuming without a powerful algorithmic approach. As one of the world’s biggest manufacturers of
   premium cars, safety and efficiency are paramount on Daimler’s production lines.

   In this competition, Daimler is challenging Kagglers to tackle the curse of dimensionality and reduce the time
   that cars spend on the test bench. Competitors will work with a dataset representing different permutations of
   Mercedes-Benz car features to predict the time it takes to pass testing. Winning algorithms will contribute to
   speedier testing, resulting in lower carbon dioxide emissions without reducing Daimler’s standards.


   **Resources**

   `Kaggle Competition Website <https://www.kaggle.com/competitions/mercedes-benz-greener-manufacturing/>`_

   `SVA Wiki-Page <https://wiki.sva.de/display/BD/DS+Challenge+-+April+2022/>`_


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   src

.. include:: modules.rst

.. automodule:: src.utils.argument_parsing
   :members:
   :undoc-members:
   :show-inheritance:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
