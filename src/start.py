#!/usr/bin/env python

import subprocess
import multiprocessing
import sys

# Import own modules
sys.path.append("./utils/argument_parsing")
import utils.argument_parsing.argument_parsing as argument_parsing


def flask_webserver():
    # subprocess.check_call(['set', 'FLASK_APP=app.py'], shell=True)
    subprocess.check_call(["flask", "run"], cwd="api", shell=True)


def dashboard():
    # subprocess.run('dir', shell=True)
    subprocess.check_call(
        ["streamlit", "run", "streamlit_dashboard.py"], cwd="dashboard/02-all_features"
    )


if __name__ == "__main__":
    args = argument_parsing.parse()
    p1 = multiprocessing.Process(name="p1", target=flask_webserver)
    p2 = multiprocessing.Process(name="p2", target=dashboard)
    p1.start()
    p2.start()
