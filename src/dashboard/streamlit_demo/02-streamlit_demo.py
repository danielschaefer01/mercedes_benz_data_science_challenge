#!/usr/bin/env python

"""
This demo illustrates Layout options
"""

import streamlit as st

st.markdown("# Layout")

st.markdown(
    "Streamlit makes it easy to organize your widgets in a left panel sidebar with st.sidebar. Each element "
    "that is passed to st.sidebar is pinned to the left, allowing users to focus on the content in your app "
    "while still having access to UI controls."
)

# Add a selectbox to the sidebar:
add_selectbox = st.sidebar.selectbox(
    "How would you like to be contacted?", ("Email", "Home phone", "Mobile phone")
)

# Add a slider to the sidebar:
add_slider = st.sidebar.slider("Select a range of values", 0.0, 100.0, (25.0, 75.0))
