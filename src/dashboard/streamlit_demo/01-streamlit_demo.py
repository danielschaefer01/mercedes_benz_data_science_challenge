#!/usr/bin/env python

import streamlit as st
import pandas as pd
import numpy as np

##############
st.markdown("# Use magic, i.e. non-streamlit methods")
# You can also write to your app without calling any Streamlit methods. Streamlit supports "magic commands," which
# means you don't have to use st.write() at all.


df = pd.DataFrame({"first column": [1, 2, 3, 4], "second column": [10, 20, 30, 40]})

df


##############
st.markdown("# Write a data frame")
# Along with magic commands, st.write() is Streamlit's "Swiss Army knife". You can pass almost anything to st.write():
# text, data, Matplotlib figures, Altair charts, and more. Don't worry, Streamlit will figure it out and render things
# the right way.

st.write("Here's our first attempt at using data to create a table:")
st.write(
    pd.DataFrame({"first column": [1, 2, 3, 4], "second column": [10, 20, 30, 40]})
)


##############
st.markdown("# Draw a line chart")

chart_data = pd.DataFrame(np.random.randn(20, 3), columns=["a", "b", "c"])

st.line_chart(chart_data)


##############
st.markdown("# Plot a map 2")
map_data = pd.DataFrame(
    np.random.randn(1000, 2) / [50, 50] + [37.76, -122.4], columns=["lat", "lon"]
)

st.map(map_data)


##############
st.markdown("# Widgets")
# When you've got the data or model into the state that you want to explore, you can add in widgets like st.slider(),
# st.button() or st.selectbox().

st.markdown("### Slider Widget")
x = st.slider("x")  # 👈 this is a widget
st.write(x, "squared is", x * x)

st.markdown("### Button Widget")
if st.button("Say hello"):
    st.write("Why hello there")
else:
    st.write("Goodbye")

st.markdown("### Select Box")
option = st.selectbox(
    "How would you like to be contacted?", ("Email", "Home phone", "Mobile phone")
)
st.write("You selected:", option)

st.markdown("### Multi-Select")
options = st.multiselect(
    "What are your favorite colors",
    ["Green", "Yellow", "Red", "Blue"],
    ["Yellow", "Red"],
)
st.write("You selected:", options)

st.markdown("### Use checkboxes to show/hide data")
if st.checkbox("Show dataframe"):
    chart_data = pd.DataFrame(np.random.randn(20, 3), columns=["a", "b", "c"])

    chart_data

st.markdown("### Use a selectbox for options")
df = pd.DataFrame({"first column": [1, 2, 3, 4], "second column": [10, 20, 30, 40]})

option = st.selectbox("Which number do you like best?", df["first column"])

"You selected: ", option


st.markdown("### Text Input")
import streamlit as st

st.text_input("Your name", key="name")

# You can access the value at any point with:
st.session_state.name


##############
st.markdown("# Display Image")

from PIL import Image

image = Image.open("../../../images/sunrise.jpg")

st.image(image, caption="Sunrise by the mountains")


##############
st.markdown("# Display Video")

video_file = open("../../../images/stars.mp4", "rb")
video_bytes = video_file.read()

st.video(video_bytes)
