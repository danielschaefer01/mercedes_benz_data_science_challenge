#!/usr/bin/env python

# Imports
import streamlit as st
import pandas as pd
import requests
import json

import matplotlib

# import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from PIL import Image

matplotlib.use("TkAgg")

# Stylings
sns.set_theme()
color = sns.color_palette()
pd.options.display.max_columns = 999
pd.options.mode.chained_assignment = None  # default='warn'

st.set_page_config(page_title="Mercedes-Benz Greener Manufacturing", page_icon="🚗")

st.title("Mercedes-Benz Greener Manufacturing")
st.markdown("## Can you cut the time a Mercedes-Benz spends on the test bench?")
st.write(
    "Mercedes Benz offers customers the option to customise their cars. To ensure the safety and reliability of each "
    "and every unique car configuration before they hit the road, Daimler’s engineers have developed a robust testing "
    "system. But optimizing the speed of their testing system for so many possible feature combinations is complex "
    "and time-consuming without a powerful algorithmic approach. As one of the world’s biggest manufacturers of "
    "premium cars, safety and efficiency are paramount on Daimler’s production lines."
)

image = Image.open("../../../images/Auto_Teststand_old.jpg")
st.image(
    image,
    caption="A test stand facilitates the objective performance evaluation of the car being tested.",
)

st.write(
    "In this competition, Daimler is challenging Kagglers to tackle the curse of dimensionality and reduce the time "
    "that cars spend on the test bench. Competitors will work with a dataset representing different permutations of "
    "Mercedes-Benz car features to predict the time it takes to pass testing. Winning algorithms will contribute to "
    "speedier testing, resulting in lower carbon dioxide emissions without reducing Daimler’s standards."
)
st.write("https://www.kaggle.com/competitions/mercedes-benz-greener-manufacturing")

st.markdown("## Enter Data")
st.markdown("#### (File is for target variables only)")


uploaded_file = st.file_uploader("Choose a file")

if uploaded_file is not None:
    file = json.load(uploaded_file)

    url = "http://192.168.178.31:5000//prediction_randomforestregressor_cleaned"
    # payload = {"X29": 1, "X47": 1, "X48": 0, "X118": 0, "X127": 1, "X261": 0, "X315": 1}
    x = requests.post(url, data=json.dumps(file))

    st.markdown("## Predicted time taken (seconds):")
    x.text
