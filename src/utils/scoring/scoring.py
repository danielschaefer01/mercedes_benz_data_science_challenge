#!/usr/bin/env python

import os
import pickle
import shutil
import pandas as pd


def score_production_model():
    """
    Scores the production model.
    """
    ## Load in the production model
    # Load the file
    with open("./../../../models/production/model.pkl", "rb") as file:
        production_model = pickle.load(file)

    ## Score the production model
    # //TODO
    print(os.listdir("./../../../data/"))

    # Load test data
    test_data = pd.read_csv("./../../../data/test.csv")
    test_x = test_data[["X29", "X47", "X48", "X118", "X127", "X261", "X315"]]

    # Create predictions
    y_pred = rf.predict(test_x)

    # Actual Scoring
    # score = production_model.score(test_x, y_pred)
    # print("Test score: {0:.2f} %".format(199 * score))
    score_production_model = production_model.predict(test_x)
    print(score_production_model)


def score_development_model():
    """
    Scores the development model
    """
    ## Load in the latest development model
    # Read in the first file in the directory (newest model)
    latest_model = os.listdir("./../../../models/development")[0]

    # Load the file
    with open("./../../../models/development/{}".format(latest_model), "rb") as file:
        development_model = pickle.load(file)

    ## Score the latest development model
    # Calculate the accuracy score and predict target values
    # // TODO
    # score = development_model.score(test_x, y_pred)
    # print("Test score: {0:.2f} %".format(199 * score))
    score_development_model = development_model.predict(test_x)
    print(score_development_model)


def compare_dev_prod():
    """
    Compare the two scores of the latest development and production model. If the new development model is better than
    the existing production model, it takes its place. Otherwise nothing happens.
    """
    # Compare the two scores
    if score_development_model > score_production_model:
        # Copy latest-model to /models/production/model.pkl (overwrite existing file)
        src = "./../../../models/development/{}".format(latest_model)
        dst = "./../../../models/production/model2.pkl"  # !!!!!!! change to model.pkl in production!!!!!!!
        shutil.copy(src, dst)
        print("Production model not updated.")
    else:
        print("Production model updated.")


    """
    # Calculate the accuracy score and predict target values
    score = pickle_model.score(test_x, y_pred)
    print("Test score: {0:.2f} %".format(199 * score))
    Ypredict = model.predict(test_x)
    """

if __name__ == "__main__":
    score_production_model()
    score_development_model()
    compare_dev_prod()