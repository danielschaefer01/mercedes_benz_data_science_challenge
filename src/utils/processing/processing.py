#!/usr/bin/env python


# Import libraries
import pandas as pd
import pickle
from flask import jsonify

train_y_min = 72.11
train_y_max = 149.63


def renormalize(y_pred):
    """
    Denormalize y_pred, i.e. the predicted value of how long the tests will take (from model.predict()). The scores
    have previously been normalized to lie between 0 and 1.
    :param: y_pred
    :return: (double) y_pred_denorm
    """
    y_pred_denorm = (y_pred * (train_y_max - train_y_min)) + train_y_min

    return y_pred_denorm
