#!/usr/bin/env python


# Import libraries
import pandas as pd
import pickle
from flask import jsonify

modelfile = "./../../models/production/model.pkl"
model = pickle.load(open(modelfile, "rb"))

# Features to select
select = ["X29", "X47", "X48", "X118", "X127", "X261", "X315"]


def testing():
    """
    Testing function.
    :return: (string) "Hello World!"
    """
    return "Hello World!"


def prep(data):
    """
    Takes the API request body. The function performs the preprocessing steps necessary before model.predict can be
    called. If the data is a single item, the data type is dict. This executes in the first loop. If the data is a
    batch, the data type will be a list of dictionaries. This is processed in the second loop.

    In Either case the filter for the desired features is applied. The data type is then converted to a pandas
    dataframe.
    :param: data
    :return: (pandas dataframe) input_features
    """
    if type(data) == dict:
        data_clean = {x: data[x] for x in select}
        input_features = pd.json_normalize(data_clean)
    elif type(data) == list:
        datalist = []
        for d in data:
            datalist.append({x: d[x] for x in select})
        input_features = pd.json_normalize(datalist)
    else:
        raise ValueError(
            "Data type is not supported. Only type(dict) and type(list of dicts) are supported."
        )

    return input_features
