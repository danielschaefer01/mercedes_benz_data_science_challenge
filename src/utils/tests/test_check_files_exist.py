#!/usr/bin/env python


import pytest
from pathlib import Path
from os.path import exists
import os


def test_model_exists():
    """
    Check whether there exists a pickled machine learning model.
    """
    file_exists = os.path.exists("./models/production/model.pkl")
    assert file_exists


def test_data_exists1():
    """
    Check wheter the sample_submission.csv file exists.
    """
    file_exists = os.path.exists("./data/sample_submission.csv")
    assert file_exists


def test_data_exists2():
    """
    Check whether the test.csv file exists.
    """
    file_exists = os.path.exists("./data/test.csv")
    assert file_exists


def test_data_exists3():
    """
    Check whether the train.csv file exists.
    """
    file_exists = os.path.exists("./data/train.csv")
    assert file_exists
