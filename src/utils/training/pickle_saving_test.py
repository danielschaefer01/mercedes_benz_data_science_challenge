#!/usr/bin/env python

import os
import pickle

import time

timestr = time.strftime("%Y%m%d-%H%M%S")
print(timestr)


"""
if not os.path.isfile("./../../models/development/test_pkl.pkl"):
    with open("./../../../models/development/test_pkl.pkl",'wb') as file:
        pickle.dump("some object", file)
"""

if not os.path.isfile("./../../models/development/test_pkl.pkl"):
    with open(
        "./../../../models/development/{}_model.pkl".format(timestr), "wb"
    ) as file:
        pickle.dump("some object", file)
