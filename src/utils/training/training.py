#!/usr/bin/env python


# Imports
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn import preprocessing
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.feature_selection import RFECV
from sklearn.ensemble import RandomForestRegressor
import xgboost as xgb
import eli5
from eli5.sklearn import PermutationImportance
import shap
import pickle
import os
import pickle
import time


"""
Feature engineering.
:return:
"""
# Import Data
train = pd.read_csv("./../../../data/train.csv")

# Drop features with 1 unique value
train.drop(
    [
        "ID",
        "X11",
        "X107",
        "X233",
        "X235",
        "X268",
        "X289",
        "X290",
        "X293",
        "X297",
        "X330",
        "X347",
    ],
    axis=1,
    inplace=True,
)

# Add some additional features. These are a combination of the categorical variables
def feature_creation(df):
    for i in ["X0", "X1", "X2", "X3", "X5", "X6", "X8"]:
        for j in ["X0", "X1", "X2", "X3", "X5", "X6", "X8"]:
            df[i + "_" + j] = df[i].astype("str") + "_" + df[j].astype("str")

    return df


train = feature_creation(train)

# NaN Check - Check for NaNs in DataFrame
if train.isnull().values.any() == False:
    pass
else:
    raise Exception(
        "The new dataframe contains NaN values. This should not be possible. Please check file."
    )

# Encode object features to numeric format
lencoders = {}
for col in train.select_dtypes(include=["object"]).columns:
    lencoders[col] = preprocessing.LabelEncoder()
    train[col] = lencoders[col].fit_transform(train[col])

# Drop duplicates
train = train.drop_duplicates()

# Delete some outliers from target feature (values greater than 150 were selected here).
train = train[(train["y"] <= 150)].reset_index(drop=True)

# Target normalization - define y_min
train_y_min = train["y"].min()
train_y_min

# Target normalization - define y_max
train_y_max = train["y"].max()
train_y_max

# Target normalization between 0 and 1 values
train["y"] = (train["y"] - train["y"].min()) / (train["y"].max() - train["y"].min())
train["y"].describe()

# Identify and remove low correlation features
# Calculate the correlation matrix (every feature - including the categorical compound features e.g. X2_X8) vs every
# other feature. Then calculate the Pearson correlation. Finally eliminate all features where the correlation threshold
# is below 0.9 (the internet said that this was a good value to initially go by).
corr_matrix = train.corr().abs().round(2)
upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))

# Threshold for removing correlated variables
threshold = 0.9

# Select columns with Pearson's correlations above threshold
collinear_features = [
    column for column in upper.columns if any(upper[column] > threshold)
]
features_filtered = train.drop(columns=collinear_features)
# print('The number of features that passed the collinearity threshold: ', features_filtered.shape[1])
features_best = []
features_best.append(features_filtered.columns.tolist())

# Updated dataframe
train = train[features_best[0]]

# Re-format the dataframe `train`
X = train.drop("y", axis=1)
y = train.y

# one add feature
X["n0"] = (X == 0).sum(axis=1)

# Normalize features between 1 and 0
scaler = preprocessing.MinMaxScaler()
X = pd.DataFrame(scaler.fit_transform(X), columns=X.columns)

# Baseline score
SEED = 12345
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.25, random_state=SEED
)
# prepare the model with target scaling
rf = RandomForestRegressor(max_depth=4, n_estimators=5, random_state=SEED)
# evaluate model
cv = KFold(n_splits=10, shuffle=True, random_state=SEED)
scores = cross_val_score(rf, X, y, scoring="r2", cv=cv, n_jobs=-1)
# summarize the result
s_mean = scores.mean()
print("Mean R2: %.3f" % (s_mean))



# Reduce Memory Usage
def reduce_memory_usage(df):
    """
    The function will reduce memory of dataframe
    Note: Apply this function after removing missing value
    :param: pandas dataframe
    :return: (pandas dataframe) dataframe
    """
    intial_memory = df.memory_usage().sum() / 1024**2
    print("Intial memory usage:", intial_memory, "MB")
    for col in df.columns:
        mn = df[col].min()
        mx = df[col].max()
        if df[col].dtype != object:
            if df[col].dtype == int:
                if mn >= 0:
                    if mx < np.iinfo(np.uint8).max:
                        df[col] = df[col].astype(np.uint8)
                    elif mx < np.iinfo(np.uint16).max:
                        df[col] = df[col].astype(np.uint16)
                    elif mx < np.iinfo(np.uint32).max:
                        df[col] = df[col].astype(np.uint32)
                    elif mx < np.iinfo(np.uint64).max:
                        df[col] = df[col].astype(np.uint64)
                else:
                    if mn > np.iinfo(np.int8).min and mx < np.iinfo(np.int8).max:
                        df[col] = df[col].astype(np.int8)
                    elif mn > np.iinfo(np.int16).min and mx < np.iinfo(np.int16).max:
                        df[col] = df[col].astype(np.int16)
                    elif mn > np.iinfo(np.int32).min and mx < np.iinfo(np.int32).max:
                        df[col] = df[col].astype(np.int32)
                    elif mn > np.iinfo(np.int64).min and mx < np.iinfo(np.int64).max:
                        df[col] = df[col].astype(np.int64)
            if df[col].dtype == float:
                df[col] = df[col].astype(np.float32)

    red_memory = df.memory_usage().sum() / 1024**2
    print("Memory usage after complition: ", red_memory, "MB")

reduce_memory_usage(train)


# Random Forest Regressor
# The Random Forest Regressor creates a random forest and then calculates the least important feature. This is then
# recursively eliminated (whilst calculating the mean squared error) until we get down to 2 features. The regressor
# will then select the model with the best error scores. This can then be used to make a model prediction.

## Feature ranking with recursive feature elimination and cross-validated selection of the best number of features
regressor = xgb.XGBRegressor()
selector = RFECV(regressor, step=1, cv=cv, n_jobs=-1, verbose=1, scoring="r2")
selector.fit(X, y)
print("The optimal number of features is {}".format(selector.n_features_))
features_rfecv = [f for f, s in zip(X, selector.support_) if s]
print("The selected features are:")
print("{}".format(features_rfecv))  ## optimal features list

## let's compare RF after feature selection
rf = RandomForestRegressor(max_depth=3, n_estimators=10, random_state=SEED)
X_rfe = X[features_rfecv]
rf.fit(X_rfe, y)
scores = cross_val_score(rf, X_rfe, y, cv=cv, scoring="r2")
print("RF based on selected dataset")
print("FR CV Accuracy Score after selection:", scores.mean().round(3))

## feature importance with eli5
rf.fit(X_rfe, y)
perm = PermutationImportance(rf, random_state=SEED).fit(X_rfe, y)
eli5.show_weights(perm, feature_names=X_rfe.columns.tolist())

# Function to calculate mean absolute error
def mae(y_true, y_pred):
    return np.mean(abs(y_true - y_pred))


baseline_guess = np.median(y)

print("The baseline guess based on Y-median value %0.2f" % baseline_guess)
print(
    "Baseline Performance based on Y-median value: MAE = %0.4f"
    % mae(y_test, baseline_guess)
)
print(
    "Baseline Performance based on Random Forest RFECV-model: MAE = %0.4f"
    % mae(y_test, rf.predict(X_test[features_rfecv]))
)


# Save to file in the development directory
timestr = time.strftime("%Y%m%d-%H%M%S")
print(timestr)

if not os.path.isfile("./../../models/development/{}_model.pkl"):
    with open(
        "./../../../models/development/{}_model.pkl".format(timestr), "wb"
    ) as file:
        pickle.dump(rf, file)  # rf = model