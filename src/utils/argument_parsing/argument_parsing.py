#!/usr/bin/env python

import argparse


def parse() -> argparse.Namespace:
    """Handles command line argument parsing.

    Returns:
        argparse.Namespace: Parsed arguments
    """
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="""\
    Data Science Challenge 2022: Mercedes-Benz Greener Manufacturing.
    
    [INFO]
    Mercedes Benz offers customers the option to customise their cars. To ensure the safety and reliability of each 
    and every unique car configuration before they hit the road, Daimler’s engineers have developed a robust testing 
    system. But optimizing the speed of their testing system for so many possible feature combinations is complex 
    and time-consuming without a powerful algorithmic approach. As one of the world’s biggest manufacturers of 
    premium cars, safety and efficiency are paramount on Daimler’s production lines.
    
    [ABOUT]
    This project provides a model training pipeline as well as a REST API and dashboard.
    
    [START]
    Run python start.py to start both the Backend API server as well as the dashboard.
    
    This project consists out of multiple parts:
    1. training: trains the machine learning model based on the Kaggle data
    2. scoring: scores the model and determines if the current model outperforms the existing one
    3. GUI/Dashboard: dashboard for interactively interacting with the model API
    4. API: Provides a REST API Interface to access the model
    """,
    )

    parser.add_argument(
        "--log",
        choices=("DEBUG", "INFO", "WARNING", "ERROR"),
        default="DEBUG",
        help="choose a logging level",
    )

    return parser.parse_args()
