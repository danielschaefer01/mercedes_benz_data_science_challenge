#!/usr/bin/env python


# Import libraries
import pandas as pd
from flask import Flask
from flask import request
from flask import jsonify
from flask import render_template
from flask_swagger_ui import get_swaggerui_blueprint
import pickle
import sys

# Import own modules
sys.path.append("../utils/processing")
import src.utils.processing.preprocessing as preprocessing
import src.utils.processing.processing as processing

app = Flask(__name__)

# Swagger setup
SWAGGER_URL = "/swagger"
API_URL = "/static/swagger.yaml"
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={"app_name": "Mercedes-Benz Greener Manufacturing"},
)
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)


@app.route("/")
def home():
    """
    Get basic information about the API service as well as the link to the interactive Swagger documentation.
    :return: (html) index.html
    """
    return render_template("index.html")


@app.route("/test")
def index():
    """
    Simple test return. This simple endpoint returns the string "Web App with Python Flask! Flask server is running!"
    :return: (string) "Web App with Python Flask! Flask server is running!"
    """
    return "Web App with Python Flask! Flask server is running!"


@app.route("/prediction_randomforestregressor_cleaned", methods=["POST"])
def predict_cleaned():
    """
    Get model prediction from API. This endpoint is for submitting the clean input data, i.e. only the features
    relevant for the model.
    :return: (json) prediction.tolist()
    """
    data = request.get_json(force=True)
    input_features = pd.json_normalize(data)
    prediction_denormalized = model.predict(input_features)
    prediction = processing.renormalize(prediction_denormalized)

    return jsonify(prediction.tolist())


@app.route("/prediction_randomforestregressor_raw", methods=["POST"])
def predict_raw_ausgelagert():
    """
    Get model prediction from API. This endpoint is for submitting the raw input data, i.e. all features.
    :return: (json) prediction.tolist()
    """
    data = request.get_json(force=True)
    input_features = preprocessing.prep(data)
    prediction_denormalized = model.predict(input_features)
    prediction = processing.renormalize(prediction_denormalized)

    return jsonify(prediction.tolist())


if __name__ == "__main__":
    print("Run directly")
    modelfile = "./../../models/production/model.pkl"
    model = pickle.load(open(modelfile, "rb"))
    app.run(host="0.0.0.0", port=5000, debug=True)
else:
    print("Run from import")
    modelfile = "./../../models/production/model.pkl"
    model = pickle.load(open(modelfile, "rb"))
    app.run(host="0.0.0.0", port=5000, debug=True)
