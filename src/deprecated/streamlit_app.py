#!/usr/bin/env python

"""
# My first app
Here's our first attempt at using data to create a table:
"""

# Imports
import streamlit as st
import pandas as pd
import tkinter
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from PIL import Image

matplotlib.use("TkAgg")

# Stylings
sns.set_theme()
color = sns.color_palette()
pd.options.display.max_columns = 999
pd.options.mode.chained_assignment = None  # default='warn'

st.title("Mercedes-Benz Greener Manufacturing")
st.markdown("## Can you cut the time a Mercedes-Benz spends on the test bench?")
st.write(
    "Mercedes Benz offers customers the option to customise their cars. To ensure the safety and reliability of each and every unique car configuration before they hit the road, Daimler’s engineers have developed a robust testing system. But, optimizing the speed of their testing system for so many possible feature combinations is complex and time-consuming without a powerful algorithmic approach. As one of the world’s biggest manufacturers of premium cars, safety and efficiency are paramount on Daimler’s production lines."
)

image = Image.open("../../images/Auto_Teststand_old.png")
st.image(
    image,
    caption="A test stand facilitates the objective performance evaluation of the car being tested.",
)

st.write(
    "In this competition, Daimler is challenging Kagglers to tackle the curse of dimensionality and reduce the time that cars spend on the test bench. Competitors will work with a dataset representing different permutations of Mercedes-Benz car features to predict the time it takes to pass testing. Winning algorithms will contribute to speedier testing, resulting in lower carbon dioxide emissions without reducing Daimler’s standards."
)
st.write("https://www.kaggle.com/competitions/mercedes-benz-greener-manufacturing")


"""
In the prior steps we performed the following steps:
* data exploration
* feature engineering
* training a machine learning model
* publishing the model as a consumable web service
"""

st.write("Let us take a look at the dataset used to create the predictions:")
input = pd.read_csv("../../data/test.csv")
input

# Load the test data set
output = pd.read_csv("../../notebooks/prediction_randomforestregressor.csv")
output

output["y"]

st.markdown("Violin plot of `y_predicted`")
# Violin plot of y_predicted
plt.figure(figsize=(12, 12))
sns.violinplot(y=output["y"])
plt.show()

st.markdown("Here is the distribution of the predicted times:")

## INSERT GRAPHIC

st.markdown("Here we see that the ......")


## TODO
## DO LIVE PREDICTION - single
## -> new page

option1 = st.selectbox("Select value for feature X29", (1, 0))
"You selected: ", option1

option2 = st.selectbox("Select value for feature X47", (1, 0))
"You selected: ", option2

option3 = st.selectbox("Select value for feature X48", (1, 0))
"You selected: ", option3

option4 = st.selectbox("Select value for feature X118", (1, 0))
"You selected: ", option4

option5 = st.selectbox("Select value for feature X127", (1, 0))
"You selected: ", option5

option6 = st.selectbox("Select value for feature X261", (1, 0))
"You selected: ", option6

option7 = st.selectbox("Select value for feature X315", (1, 0))
"You selected: ", option7

# inputdf = pd.DataFrame[option1[0]] #, option2, option3, option4, option5, option6, option7]
# inputdf

list1 = [option1, option2, option3, option4, option5, option6, option7]
list1
st.write(type(list1))
st.write(np.shape(list1))

user_df = pd.DataFrame(list1, columns=["inputs"])
user_df


# make API call

# display API call result
# Plot API Call result
