#!/usr/bin/env python

# Imports
import streamlit as st
import pandas as pd
import requests
import json

# import tkinter
import matplotlib

# import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from PIL import Image

matplotlib.use("TkAgg")

# Stylings
sns.set_theme()
color = sns.color_palette()
pd.options.display.max_columns = 999
pd.options.mode.chained_assignment = None  # default='warn'

st.set_page_config(page_title="Mercedes-Benz Greener Manufacturing", page_icon="🚗")

st.title("Mercedes-Benz Greener Manufacturing")
st.markdown("## Can you cut the time a Mercedes-Benz spends on the test bench?")
st.write(
    "Mercedes Benz offers customers the option to customise their cars. To ensure the safety and reliability of each and every unique car configuration before they hit the road, Daimler’s engineers have developed a robust testing system. But, optimizing the speed of their testing system for so many possible feature combinations is complex and time-consuming without a powerful algorithmic approach. As one of the world’s biggest manufacturers of premium cars, safety and efficiency are paramount on Daimler’s production lines."
)

image = Image.open("../../images/Auto_Teststand_old.png")
st.image(
    image,
    caption="A test stand facilitates the objective performance evaluation of the car being tested.",
)

st.write(
    "In this competition, Daimler is challenging Kagglers to tackle the curse of dimensionality and reduce the time that cars spend on the test bench. Competitors will work with a dataset representing different permutations of Mercedes-Benz car features to predict the time it takes to pass testing. Winning algorithms will contribute to speedier testing, resulting in lower carbon dioxide emissions without reducing Daimler’s standards."
)
st.write("https://www.kaggle.com/competitions/mercedes-benz-greener-manufacturing")

st.markdown("## Enter Data")


with st.form("my_form"):

    option1 = st.selectbox("Select value for feature X29", (1, 0))
    "You selected: ", option1

    option2 = st.selectbox("Select value for feature X47", (1, 0))
    "You selected: ", option2

    option3 = st.selectbox("Select value for feature X48", (1, 0))
    "You selected: ", option3

    option4 = st.selectbox("Select value for feature X118", (1, 0))
    "You selected: ", option4

    option5 = st.selectbox("Select value for feature X127", (1, 0))
    "You selected: ", option5

    option6 = st.selectbox("Select value for feature X261", (1, 0))
    "You selected: ", option6

    option7 = st.selectbox("Select value for feature X315", (1, 0))
    "You selected: ", option7

    #   submitted = st.form_submit_button("Submit")

    # Every form must have a submit button.
    submitted = st.form_submit_button("Submit")
    if submitted:
        st.write(
            "Feature 1",
            option1,
            "Feature2",
            option2,
            "Feature 3",
            option3,
            "Feature 4",
            option4,
            "Feature5",
            option5,
            "Feature6",
            option6,
            "Feature7",
            option7,
        )

# inputdf = pd.DataFrame[option1[0]] #, option2, option3, option4, option5, option6, option7]
# inputdf

list1 = [option1, option2, option3, option4, option5, option6, option7]
list1
st.write(type(list1))
st.write(np.shape(list1))

user_df = pd.DataFrame(list1, columns=["inputs"])
user_df


# make API call

# display API call result
# Plot API Call result


def fetch(session, url):
    try:
        result = session.get(url)
        return result.json()
    except Exception:
        return {}


session = requests.Session()

# with st.form("my_form"):
#    index = st.number_input("ID", min_value=0, max_value=100, key="index")
#    submitted = st.form_submit_button("Submit")

if submitted:
    st.write("Result")
    data = fetch(session, f"https://picsum.photos/id/{index}/info")
    if data:
        st.image(data["download_url"], caption=f"Author: {data['author']}")
    else:
        st.error("Error")


uploaded_file = st.file_uploader("Choose a file")
if uploaded_file is not None:

    # To read file as bytes:
    bytes_data = uploaded_file.getvalue()
    st.write(bytes_data)

    # To convert to a string based IO:
    stringio = StringIO(uploaded_file.getvalue().decode("utf-8"))
    st.write(stringio)

    # To read file as string:
    string_data = stringio.read()
    st.write(string_data)

    # Can be used wherever a "file-like" object is accepted:
    dataframe = pd.read_csv(uploaded_file)
    st.write(dataframe)


url = "http://192.168.178.31:5000//prediction_randomforestregressor"
payload = {"X29": 1, "X47": 1, "X48": 0, "X118": 0, "X127": 1, "X261": 0, "X315": 1}
x = requests.post(url, data=json.dumps(payload))  # , headers=headers)
print(x)
print(x.content)
print(x.text)

print(x.json())
x_dict = x.json()
print(x_dict)

payload = json.dumps(uploaded_file)
payload
