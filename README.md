# Daniel Schaefer Data Science Challenge

[![pipeline status](https://codehub.sva.de/Daniel.Schaefer/ds_challenge_daniel_schaefer/badges/main/pipeline.svg)](https://codehub.sva.de/Daniel.Schaefer/ds_challenge_daniel_schaefer/-/pipelines)
[![documentation](https://img.shields.io/badge/documentation-here%20on%20GitLab-brightgreen.svg)](https://codehub.sva.io/Daniel.Schaefer/ds_challenge_daniel_schaefer)

## :information_source: About
### Mercedes-Benz Greener Manufacturing
#### Can you cut the time a Mercedes-Benz spends on the test bench?
Mercedes Benz offers customers the option to customise their cars. To ensure the safety and reliability of each 
and every unique car configuration before they hit the road, Daimler’s engineers have developed a robust testing 
system. But optimizing the speed of their testing system for so many possible feature combinations is complex 
and time-consuming without a powerful algorithmic approach. As one of the world’s biggest manufacturers of 
premium cars, safety and efficiency are paramount on Daimler’s production lines.

![img_2.png](img_2.png)
<div align="center">

##### A test stand facilitates the objective performance evaluation of the car being tested.
</div>

In this competition, Daimler is challenging Kagglers to tackle the curse of dimensionality and reduce the time 
that cars spend on the test bench. Competitors will work with a dataset representing different permutations of 
Mercedes-Benz car features to predict the time it takes to pass testing. Winning algorithms will contribute to 
speedier testing, resulting in lower carbon dioxide emissions without reducing Daimler’s standards.

### Kaggle Competition Website:
https://www.kaggle.com/competitions/mercedes-benz-greener-manufacturing


### SVA Wiki-Page:
https://wiki.sva.de/display/BD/DS+Challenge+-+April+2022

## :package: Installation

To install this package directly from GitLab, run:

```bash
# make sure you have pip installed
python3 -m pip install --user -U git+https://codehub.sva.de/Daniel.Schaefer/ds_challenge_daniel_schaefer

```

## :blue_book: Documentation

Code Documentation can be found [here on GitLab](https://codehub.sva.io/Daniel.Schaefer/ds_challenge_daniel_schaefer).

API Documentation can be found by accessing [http://localhost:5000/swagger](http://localhost:5000/swagger). 
Note that the flask webserver needs to be started for this to work.

## :thumbsup: Getting Started

Run `start.py` file in `\src` folder.

This starts both the flask web server as well as the dashboard. Both services can still be 
started independently of the `start.py` script.


## :computer: Flask Web Server
The flask web server provides a REST API for the machine learning model. The python file for the flask web server can 
be found at `./src/api/app.py`.

To run the flask web server run:

`cd api` \
`set FLASK_APP=app.py` \
`$env FLASK_APP=app.py` \
`flask run`


The following endpoints are available:

`/` \
Default Endpoint. This returns basic information on the available endpoints and where to find the code repository.

`/test` \
Endpoint for testing connectivity.

`/prediction_randomforestregressor_cleaned` \
Endpoint for accessing machine learning model using just the target variables. An example input is:
```
{
    "X29": 1,
    "X47": 1,
    "X48": 0,
    "X118": 0,
    "X127": 1,
    "X261": 0,
    "X315": 1
}
```

`/prediction_randomforestregressor_raw` \
Endpoint for accessing machine learning model using all unfiltered variables. An example can be found at 
`./src/dashboard/02-all_features/input_all.json`.



## :chart_with_upwards_trend: Dashboard
There are two versions of the dashboard. In each,the user can submit a JSON file and the model will return a prediction.
The first version (`./src/dashboard/01-target_features`) takes a file with just the target variables (see `./src/dashboard/01-target_features/input_single.json` for an example) whilst the second 
version (`./src/dashboard/02-all_features`) takes a file with all the features (see `./src/dashboard/02-all_features/input_all.json` for an example).

**To start dashboard** \
`cd ./src/dashboard/02-all_features` \
`streamlit run streamlit_dashboard.py`

Navigate to URL in browser. \
Upload JSON file.



## :clapper: Initialize Project
When you first initialise the project, no model file may be present. This requires model training. 

Run the `/src/utils/training/training.py` script to generate a machine learning model (a pickle file). The default 
location is `/models/development/<CURRENT-TIMESTAMP>_model.pkl`.

Run the `/src/utils/scoring/scoring.py` script. This will compare the current best model at 
`/models/production/model.pkl` (if present) to the most current model in `/models/development`. If the new development 
model has a better score than the production model, it replaces the current production model.

Note that the folder `/models/development` is not available on the prodcution/main branch and can instead be found in 
the development branch.